package com.jaguarlabs.rsa;

import android.util.Base64;
import android.util.Log;

import org.spongycastle.util.io.pem.PemObject;
import org.spongycastle.util.io.pem.PemWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSAEncription {

    //This is the provider from Spongycastle, needed for generating the RSA keypair
    static {
        Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
    }

    private KeyPair keyPair;

    /***
     * The default constructor generates the keipair whit a keysize of 1024 bits.
     */
    public RSAEncription (){
        keyPair=generateRSAKeys(0);
    }

    /***
     * This constructor generates the KeyPair, recieves the keysize to generate the keypair.
     * The common sizes are: 256, 512, 1024, 2048.
     * @param bitValue
     */
    public RSAEncription(int bitValue){
        keyPair=generateRSAKeys(bitValue);
    }

    /***
     * This constructor generates the KeyPair, takes the public and private keys from the strings passed as parameters.
     * @param publicKey
     * @param privateKey
     */
    public RSAEncription(String publicKey, String privateKey){
        keyPair=new KeyPair(stripPublicKeys(publicKey), stripPrivateKeys(privateKey));
    }

    /***
     * This method strips the headers from the privatekey, if the header doesn't have the \n then you should erase it from the replace() method.
     * @param privateKey
     * @return clean privatekey
     */
    private PrivateKey stripPrivateKeys(String privateKey){
        try {
            String privKeyPEM = privateKey.replace("-----BEGIN RSA PRIVATE KEY-----\n", "");
            privKeyPEM = privKeyPEM.replace("-----END RSA PRIVATE KEY-----", "");
            System.out.println(privKeyPEM);
            byte[] encoded = Base64.decode(privKeyPEM, Base64.DEFAULT);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * This method strips the headers from the publickey, if the header doesn't have the \n then you should erase it from the replace method.
     * @param publicKey
     * @return clean publickey
     */
    private PublicKey stripPublicKeys(String publicKey){
        try {
            String publicKeyString = publicKey.replace("-----BEGIN PUBLIC KEY-----\n", "");
            publicKeyString = publicKeyString.replace("-----END PUBLIC KEY-----", "");
            System.out.println(publicKeyString);
            byte[] encoded = Base64.decode(publicKeyString, Base64.DEFAULT);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(encoded);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(X509publicKey);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Used to decrypt the text sent as the parameter.
     * @param text
     * @return decrypted text
     */
    public String decryptText(String text){
        try {
            final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
            byte[] ciphertextBytes = Base64.decode(text.getBytes(), Base64.DEFAULT);
            byte[] decryptedBytes = cipher.doFinal(ciphertextBytes);
            String decryptedString = new String(decryptedBytes,"UTF-8");
            Log.d("TEXTO DESENCRIPTADO", decryptedString);
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * Used to encrypt the text sent as the parameter.
     * @param text
     * @return encripted text
     */
    public String encryptText(String text){
        try {
            final Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

            // ENCRYPT using the PUBLIC key
            cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
            byte[] encryptedBytes = cipher.doFinal(text.getBytes());
            String chipertext = new String(Base64.encode(encryptedBytes, Base64.DEFAULT));

            Log.d("TEXTO ENCRIPTADO", chipertext);

            return chipertext;

        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * This method generates the RSA keypair, recieves an integer that represents the key size in bits.
     * The common sizes are: 256, 512, 1024, 2048.
     * @param bits
     * @return a KeiPair
     */
    private KeyPair generateRSAKeys(int bits){
        int bitValue=1024;
        if (bits!=0){
            bitValue=bits;
        }
        try {
            SecureRandom random = new SecureRandom();
            RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(bitValue, RSAKeyGenParameterSpec.F4);
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "SC");
            generator.initialize(spec, random);
            return generator.generateKeyPair();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /***
     * This method prints into the logcat, the RSA public and private keys generated after intanciating this class
     */
    public void printRSAKeys(){
        StringWriter publicStringWriter;
        StringWriter privateStringWriter;
        try {
            publicStringWriter = new StringWriter();
            PemWriter pemWriter = new PemWriter(publicStringWriter);
            pemWriter.writeObject(new PemObject("PUBLIC KEY", keyPair.getPublic().getEncoded()));
            pemWriter.flush();
            pemWriter.close();
            Log.d("LLAVE PUBLICA", publicStringWriter.toString());
        } catch (IOException e) {
            Log.e("RSA", e.getMessage());
            e.printStackTrace();
        }

        try{
            privateStringWriter = new StringWriter();
            PemWriter pemWriter = new PemWriter(privateStringWriter);
            pemWriter.writeObject(new PemObject("PRIVATE KEY", keyPair.getPrivate().getEncoded()));
            pemWriter.flush();
            pemWriter.close();
            Log.d("LLAVE PRIVADA", privateStringWriter.toString());
        } catch (IOException e) {
            Log.e("RSA", e.getMessage());
            e.printStackTrace();
        }
    }
}