package com.jaguarlabs.rsa;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

public class MainActivity extends ActionBarActivity {

    private static final String privateKeyString =
            "-----BEGIN RSA PRIVATE KEY-----\n" +
                    "MIICWwIBAAKBgFuJdSOJdHdytMMlhSNEAGlCj/Nn+NIkURZz/FjJDvk27I4HYo0a\n" +
                    "fAT2g4lhIF4RZCcLyxsAYGvqCQK3EpJC2L1d0sbCoF33PFO7qgPv9WWdjmhzQaIN\n" +
                    "jyTEpIro88IO4ewesxtLGmRKqihI/FdhWaBpMM+Rk1X7aiJwIxo5dEtTAgMBAAEC\n" +
                    "gYBJmU4cEqsYV5xLw+E2p7iE6cYAebqwjA6+tOdTI+XsL2MoF3v/5CmPe/3q+bMi\n" +
                    "r20NkAfwX+8nFXfFPRmrjB/juhTKi9EmnlyTkMWtMP7jP/THQKDb+zEOGrgzTEjb\n" +
                    "j16uMKvFw8ZeckXxgsYqNfZYSmDAbYEe/KsoQy8+BUfV0QJBAK9yxc11kMDIg72h\n" +
                    "dMmPbajO41kikuTHy6OscTQ+51nGV6BLr+IHQokE1foyONcI39RE3BUqljyiLbwf\n" +
                    "1BX1c2cCQQCFkDRB+jeU0p3R0UNMxyvHlfdmWzOEohu7QW46JtvGcNWlWQl9cJf0\n" +
                    "SUJKoehhSi7F1FEVYzfLF1W/6voYUAE1AkBfoNYxk5fvjBtKW9t1FUk3DnUam16n\n" +
                    "dkMHljEpwfOFmjcetsJKF5zrZAhhU8hpZHvjjhjLLKgwqlD1Tf/ibCOXAkAIpjH8\n" +
                    "I8v9ozX9JGQLUMqQ/jErO7S2XhkVo0MUGPgafawYLQ/kQ6pTc3sLgjieNEObJAis\n" +
                    "LeSrDjPLXdgcQvIRAkEAghgnDFI0pVyOgt6jba+MDtpho/yuiS7s1koQ3B2nKfrt\n" +
                    "ggLKt05yl+e44+5jWM70eubtjQIof7PxXydLJyklaA==\n" +
                    "-----END RSA PRIVATE KEY-----";
    private static final String publicKeyString =
            "-----BEGIN PUBLIC KEY-----\n" +
                    "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgFuJdSOJdHdytMMlhSNEAGlCj/Nn\n" +
                    "+NIkURZz/FjJDvk27I4HYo0afAT2g4lhIF4RZCcLyxsAYGvqCQK3EpJC2L1d0sbC\n" +
                    "oF33PFO7qgPv9WWdjmhzQaINjyTEpIro88IO4ewesxtLGmRKqihI/FdhWaBpMM+R\n" +
                    "k1X7aiJwIxo5dEtTAgMBAAE=\n" +
                    "-----END PUBLIC KEY-----";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RSAEncription rsaEncription = new RSAEncription(publicKeyString, privateKeyString);
        String name=rsaEncription.encryptText("Prueba de encriptacion de android");
        rsaEncription.decryptText(name);
    }
}
